use serenity::{prelude::*, model::prelude::*, utils::MessageBuilder};
use rand::{Rng, seq::IteratorRandom};

#[derive(Clone)]
struct Marko {
    greeting: &'static str,
    citations: &'static[&'static str],
    reactions: &'static[&'static str],
}

fn random_emoji(guild: &Guild, names: &'static[&'static str]) -> Emoji {
    guild.emojis
        .values()
        .filter(|e| names.contains(&&e.name.as_str()))
        .choose(&mut rand::thread_rng())
        .unwrap()
        .clone()
}

impl EventHandler for Marko {
    fn ready(&self, ctx: Context, data: Ready) {
        let self_clone = self.clone();

        std::thread::spawn(move || {
            for guild in &data.guilds {
                match guild {
                    GuildStatus::OnlineGuild(guild) => {
                        if let Some(chan) = guild.default_channel(data.user.id) {
                            let chan = chan.read();
                            chan.say(&ctx.http, format!("{} {}", self_clone.greeting, random_emoji(&guild, &self_clone.reactions).mention())).ok();
                        }
                    },
                    _ => {}
                }
            }


            loop {
                if rand::thread_rng().gen_range(0, 60 * 6) == 12 {
                    for guild in &data.guilds {
                        match guild {
                            GuildStatus::OnlineGuild(guild) => {
                                if let Ok(chans) = guild.channels(&ctx.http) {
                                    let quote = if rand::random() {
                                        MessageBuilder::new()
                                            .push(self_clone.citations.iter().choose(&mut rand::thread_rng()).unwrap_or(&""))
                                            .emoji(&random_emoji(&guild, &self_clone.reactions))
                                            .build()
                                    } else {
                                        self_clone.citations.iter().choose(&mut rand::thread_rng()).unwrap_or(&"").to_string()
                                    };

                                    match chans.values().choose(&mut rand::thread_rng()) {
                                        Some(chan) => { chan.say(&ctx.http, quote).ok(); }
                                        _ => {},
                                    }
                                }
                            },
                            _ => {}
                        }
                    }
                }
                std::thread::sleep(std::time::Duration::from_millis(1000 * 60));
            }
        });
    }

    fn message(&self, ctx: Context, msg: Message) {
        let mentionned = match ctx.http.get_current_user() {
            Ok(bot) => msg.mentions.iter().any(|u| u.id == bot.id),
            _ => false,
        };

        if msg.content == "Tu sais ce qui se passe aujourd'hui Marko ?" {
            msg.channel_id.say(&ctx.http, "Oui ! C'est l'anniversaire d'Éloïse ! Joyeux anniversaire !").ok();
            return;
        }

        if mentionned {
            let quote = if rand::random() {
                MessageBuilder::new()
                    .push(self.citations.iter().choose(&mut rand::thread_rng()).unwrap_or(&""))
                    .push(" ")
                    .emoji(&random_emoji(&msg.guild(&ctx.cache).unwrap().read(), &self.reactions))
                    .build()
            } else {
                self.citations.iter().choose(&mut rand::thread_rng()).unwrap_or(&"").to_string()
            };

            msg.channel_id.say(&ctx.http, quote).ok();
        }

        if rand::thread_rng().gen_range(0, 15) == 12 {
            let emoji = random_emoji(&msg.guild(&ctx.cache).unwrap().read(), &self.reactions);
            msg.react(ctx.http.clone(), emoji).ok();
        }

        let guild = msg.guild(&ctx.cache).unwrap();
        let guild = guild.read();
        if let Some(emoji) = guild.emojis.values().find(|e| e.name == "bisexual_flag") {
            if msg.content.to_lowercase().contains("bi") {
                msg.react(ctx.http.clone(), emoji.clone()).ok();
            }
        }

        if let Some(emoji) = guild.emojis.values().find(|e| e.name == "transgender_flag") {
            if msg.content.to_lowercase().contains("trans") {
                msg.react(ctx.http.clone(), emoji.clone()).ok();
            }
        }

        if let Some(emoji) = guild.emojis.values().find(|e| e.name == "appreciable") {
            if msg.content.to_lowercase().contains("pan") {
                msg.react(ctx.http.clone(), emoji.clone()).ok();
            }
        }

        if rand::thread_rng().gen_range(0, 30) == 0 && msg.author.id.0 == 538834178888695869 {
            msg.channel_id.say(&ctx.http, &[
                "Mais Johan, ça serait pas ton anniversaire ?",
                "Bon anniversaire Johan !",
                "Joyeuuuuxxx anniiiiversaiiirreee\nJoyeuuuuxxx anniiiiversaiiirreee\nJoyeuuuuxxx anniiiiversaiiirreee Johaannn\nJoyeuuuuxxx anniiiiversaiiirreee !",
                "Johan, je viens d'apprendre que c'était ton anniversaire aujourd'hui !",
                "Johan, tu nous avait caché que c'était ton anniversaire !",
                "Tout le monde souhaite un bon anniversaire à Johan !",
                "Happy Birthday to you, my dear Johan !",
                "Devinez qui fête son anniversaire…",
                "J\nOyeux\nH\nA\nNniversaire",
            ].iter().choose(&mut rand::thread_rng()).unwrap_or(&"")).ok();
        }

        if rand::thread_rng().gen_range(0, 50) == 0 && msg.author.id.0 == 102076324067172352 {
            msg.channel_id.say(&ctx.http, &[
                "Edgar, t'as perdu.",
                "Je suis au regret de vous annoncer que vous avez perdu.",
                "Je possède une affiche jaune.",
                "Tu veux venir dans ma Clio ?",
                "Pissenlit.",
                "P E R D U",
                "Perdu hurluberlu !",
                "EeDddDgGgAAarRrDD, tOuUsShh dUu JaUunNeeeE!!!",
                "Salut, j'ai perdu.",
                "U D R E P",
                "UGVyZHUuCg==",
                "Tu sais ce que je vais dire…",
                "T'es allé à la BIS aujourd'hui j'espère ?"
            ].iter().choose(&mut rand::thread_rng()).unwrap_or(&"")).ok();
        }

        if msg.content.to_lowercase().contains("l")
        && msg.content.to_lowercase().contains("g")
        && msg.content.to_lowercase().contains("b")
        && msg.content.to_lowercase().contains("t") {
            msg.react(
                ctx.http.clone(),
                ReactionType::Unicode("🏳️‍🌈".to_owned())
            ).map_err(|e| {
                println!("{:?}", e)
            }).ok();
        }
    }
}

fn main() {
    let mut client = Client::new(std::env::var("TOKEN").unwrap(), Marko {
        greeting: "Bonjour. Je suis biologiste. Ça pulse ?",
        citations: &[
            "Ça pulse !",
            "Ça pulse ?",
            "C'est une mine d'information.",
            "Je suis biologiste.",
            "Vive les OGMs.",
            "Ça, espérons-le, ça vous parle !?!",
            "Tu vas finir dans le champ de maïs !",
            "Aujourd'hui, je suis énervé !",
            "Anyone interested in Dungeon and Dragons ?",
            "Le nucléaire, c'est pas si mal.",
            "Je suis un fan de la fusion !",
            "Trop bon, et meilleur encore quand on entend la réaction de ce connard de Dupont-Moretti 🤣 ! Et maintenant... Nicolas, Nicolas, Nicolas, ... !!!",
            "Cinq ans déjà, et rien ne change, ou alors en pire…",
            "Ségolène Royal sur Inter ce matin... dire qu'un jour de 2007, j'ai voté pour ça... 🤮",
            "La place de cette ordure est en prison !",
            "Merci Pascal !!!",
            "Un état néo-fasciste, ça donne ça !",
            "Vive les bactéries chimioluminescentes !",
            "Bien dit !",
        ],
        reactions: &[
            "marko",
            "markoccinelle",
            "maaaaaaarkowicz",
            "marko_terminator",
            "ssa_pulsseu",
            "marko_joy",
            "ca_pulse",
        ],
    }).expect("Discord login error");

    if let Err(why) = client.start() {
        println!("Client error: {:?}", why);
    }
}
